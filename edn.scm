(module edn

(read-edn
 edn-parsers
 default-edn-parsers
 edn-tag-parsers
 default-edn-tag-parsers
 register-edn-parser!
 register-edn-tag-parser!
 parse-unknown-edn-tags?
 write-edn
 write-edn/tag
 edn-emitters
 default-edn-emitters
 register-edn-emitter!)

"edn-impl.scm"

)
