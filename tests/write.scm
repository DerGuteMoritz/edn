(define-syntax test-write
  (syntax-rules ()
    ((_ in out)
     (test (with-output-to-string (lambda () (write in)))
           out
           (with-output-to-string (lambda () (write-edn in)))))))

(test-begin "writing")

(test-group "atoms"
  (test-write foo: ":foo")
  (test-write 'foo "foo")
  (test-write 'foo/bar "foo/bar")
  (test-write 123 "123")
  (test-write "hey\na string!" "\"hey\\na string!\"")
  (test-write #t "true")
  (test-write #f "false")
  (test-write 'nil "nil"))

(test-group "collections"
  (test-write '(list a "b" c:) "(a \"b\" :c)")
  (test-write '(vector (vector x)) "[[x]]")
  (test-write '(map (foo: . 1) (bar: . 2)) "{:foo 1 :bar 2}")
  (test-write '(set x y z) "#{x y z}"))

(test-group "tags"
  (test-write '(tag (#f . inst) "1985-04-12T23:20:50.52Z")
              "#inst \"1985-04-12T23:20:50.52Z\"")
  (test-write '(tag (#f . uuid) "f81d4fae-7dec-11d0-a765-00a0c91e6bf6")
              "#uuid \"f81d4fae-7dec-11d0-a765-00a0c91e6bf6\"")
  (test-write '(tag (my . tag) 123)
              "#my/tag 123"))

(test-group "custom emitters"
  (define-record foo bar)
  (register-edn-emitter! foo?
                         (lambda (x out)
                           (write-edn/tag 'my 'foo (foo-bar x) out)))
  (test-write (make-foo "this is bar") "#my/foo \"this is bar\""))

(test-end)
